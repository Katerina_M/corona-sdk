-- 
-- Отрисовка изображений на экране
--
local scrollViewImage = {}
scrollViewImage.isDebug = true

local pJ = require ( "parseJSON" )
local arrayImage = pJ.parseFile ('images.json')

local widget = require("widget")

local fileName
local y = 0

-- ScrollView listener
local function scrolListener ( event )
  local phase = event.phase
  local direction = event.derection
  
  --Если ScrollView дошёл до предела прокрутки
  if event.limitReached then
    if "up" == direction then
      print ("Достигнут верхний предел")
    elseif "down" == direction then
      print ("Достигнут нижний предел")
    end
  end
  return true
end

--Создание ScrollView
local scrollView = widget.newScrollView
{
  left = 0,
  top = 0,
  width = display.contentWidth,
  height = display.contentHeight,
  topPadding = 50,
  bottomPadding = 50,
  horizontalScrollDisabled = true,
  verticalScrollDisabled = false,
  hideBackground = true, 
  listener = scrollListener,  
}

--Отрисовка изображения
local function rectImage (fname)
    local image = display.newImageRect( fname,system.TemporaryDirectory, 200, 100 )
    image.x = display.contentCenterX
    image.y = y
    y=y+150
    scrollView:insert( image )
    return
end

  local function networkListener( event )
    if ( event.isError ) then
        print ( "Network error - ошибка загрузки" )
    elseif ( event.phase == "began" ) then
        print( "Фаза прогресса: began" )
    elseif ( event.phase == "ended" ) then
        print( "загрузка" )
        fileName = event.response.filename
        rectImage (fileName)
    end
end

--Проверка файла на существование
local function doesFileExist( fname, path )
		local results = false
		local filePath = system.pathForFile( fname, path )
		if filePath then
			filePath = io.open( filePath, "r" )
		end
		if  filePath then
			-- Закрытие файла
			filePath:close()
			results = true
		else
			print( "Файл не существует -> " .. fname )
		end
		return results
	end

--Вывод скролла с изображениями на экран
function scrollViewImage.scrollImage ( )
  for i,v in pairs(arrayImage) do
    fileName = tostring(i)..".png"
    if doesFileExist(fileName, system.TemporaryDirectory) then --Если файлы загружены, выводим на экран  
      rectImage (fileName)
    else
      --Загрузка изображений из файла JSON
      local params = {} 
      network.download(
        tostring(arrayImage[i]),
        "GET",
        networkListener,
        params,
        fileName,
        system.TemporaryDirectory
      )
    end
  end
return
end

return scrollViewImage