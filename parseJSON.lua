-- 
-- Парсинг файла JSON
--
local parseJSON = {}
parseJSON.isDebug = true

local json = require( "json" )
 
 -- Чтение файла
function parseJSON.loadValue(fname)
    local fileName = fname
    local path = system.pathForFile( fileName, system.ResourceDirectory )
      print( path )
    local file = io.open( path, "r" ) 
  if file then -- Проверка существования файла
    local contents = file:read( "*a" )
    io.close( file )
    return contents -- Функция фозвращает переменную с данными из файла
  else
    return ''
  end
end

-- Парсинг данных, загруженных из файла
function parseJSON.parseFile (fname)
    local fileName = fname
    local t = json.decode(parseJSON.loadValue(fileName)) -- 
    local arrayImage = {} -- Массив со значениями из файла

    local serializedString = json.encode( t , { indent = true })
 
    local decoded, pos, msg = json.decode( serializedString )
  if not decoded then
    print( "Decode прошёл с ошибкой "..tostring(pos)..": "..tostring(msg) )
  else
    -- Читаем строки из файла по значению image
    for i,v in pairs(decoded.images) do
      arrayImage[i] = decoded.images[i]
      print( decoded.images[i] )  
    end
  end
return arrayImage --Возвращаем массив со значениями из объекта image
end 

return parseJSON