-- 
-- Создание сцены
--
local composer = require( "composer" )
local scene = composer.newScene()

local sVI = require ( "scrollViewImage" )

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    
    if ( phase == "did" ) then
      sVI.scrollImage ()
    end
end

-- Listener настройка
scene:addEventListener( "show", scene )

return scene